import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    private consoleUrl = environment.console.url;

    constructor(private http: HttpClient) {
    }

    public getUsers(userId: string): Observable<any> {
        const endpoint = this.consoleUrl + '/users'
            + '?userId=' + userId;
        return this.http.get<any>(endpoint);
    }
}
