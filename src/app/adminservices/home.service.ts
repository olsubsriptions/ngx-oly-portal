import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class HomeService {
  private consoleUrl = environment.console.url;

  constructor(private http: HttpClient) {
  }

  getConsole(uid: any): Observable<any> {
    return this.http.get<any>(`${this.consoleUrl}/users/${uid}`);
  }
}
