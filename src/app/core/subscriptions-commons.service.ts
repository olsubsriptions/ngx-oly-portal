import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionsCommonsService {
  apiUrl = environment.api.url;

  constructor(private http: HttpClient) {
  }

  getUserName(loginData: any) {
    // return this.http.post<any>(`${this.apiUrl}/common/validate/user`, {login: loginData});
    return this.http.post<any>(`${this.apiUrl}/users/validate/user`, loginData);
  }

  getCountryCodes() {
    return this.http.get<Country[]>(`${this.apiUrl}/countries`);
  }
}
