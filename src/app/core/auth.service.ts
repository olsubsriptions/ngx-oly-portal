import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth, User } from 'firebase/app';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: User = null;

  constructor(public angularFireAuth: AngularFireAuth) {
    this.angularFireAuth.authState.subscribe(user => {
      if (user) {
        this.user = user;
        localStorage.setItem('firebase-user', JSON.stringify(this.user));
      } else {
        this.user = null;
        localStorage.removeItem('firebase-user');
      }
    });
  }

  login(email: string, password: string) {
    return this.angularFireAuth.auth.signInWithEmailAndPassword(email, password);
  }

  async logout() {
    await this.angularFireAuth.auth.signOut();
    localStorage.removeItem('firebase-user');
  }
  get authenticated(): boolean {
    console.log(this.user);
    return this.user !== null;
  }
}
