import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateRequirementComponent } from './generate-requeriment.component';

describe('GenerateRequirementComponent', () => {
  let component: GenerateRequirementComponent;
  let fixture: ComponentFixture<GenerateRequirementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateRequirementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateRequirementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
