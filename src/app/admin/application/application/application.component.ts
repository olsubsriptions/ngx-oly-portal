import { Component, OnInit } from '@angular/core';
import { User } from 'firebase';
import { ApplicationBySubscriptionService } from 'src/app/adminservices/applicationBySubscription.service';
import { ApplicationSubscription } from 'src/app/class/applicationSubscription';
import { ApplicationCode } from 'src/app/class/enums';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-application',
    templateUrl: './application.component.html',
    styleUrls: ['./application.component.scss']
})
export class ApplicationComponent implements OnInit {

    public user: User;
    public listAppSubscription = new Array<ApplicationSubscription>();
    public appSubscription: ApplicationSubscription;
    public applicationCode = ApplicationCode;

    constructor(
        private applicationBySubscriptionService: ApplicationBySubscriptionService,
        private router: Router,
        private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.user = JSON.parse(localStorage.getItem('firebase-user'));
        this.getApplicationsBySubscription();
    }

    public getApplicationsBySubscription() {

        this.applicationBySubscriptionService.getApplicationsBySubscription(this.user.uid).subscribe(
            (instance: any) => {
                this.listAppSubscription = instance;
            }
        );
    }

    public applicationDetail(appSubscription: ApplicationSubscription) {
        const appSubscriptionId = appSubscription.w0791;
        this.applicationBySubscriptionService.getApplicationSubscription(appSubscriptionId).subscribe(
            (instance: any) => {
                this.appSubscription = instance;
                switch (this.appSubscription.w0799) {
                    case this.applicationCode.MONETA:
                        this.router.navigate(['../active/mnt/', appSubscriptionId], {relativeTo: this.route});
                        break;
                    case this.applicationCode.SECURITY_PROTECCION_SUITE:
                        this.router.navigate(['../active/sps/', appSubscriptionId], {relativeTo: this.route});
                        break;
                    case this.applicationCode.TRANSLINK_TRANSACTION_SERVICES:
                        this.router.navigate(['../active/tts/', appSubscriptionId], {relativeTo: this.route});
                        break;
                }

            }
        );
    }

}
