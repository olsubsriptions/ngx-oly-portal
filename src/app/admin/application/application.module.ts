import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ApplicationRoutingModule } from './application-routing.module';
import { ApplicationComponent } from './application/application.component';
import { PurchaseComponent } from './purchase/purchase.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { SpsDetailComponent } from './application-detail/sps-detail/sps-detail.component';
import { TtsDetailComponent } from './application-detail/tts-detail/tts-detail.component';
import { MntDetailComponent } from './application-detail/mnt-detail/mnt-detail.component';


@NgModule({
  declarations: [
    ApplicationComponent,
    PurchaseComponent,
    SpsDetailComponent,
    TtsDetailComponent,
    MntDetailComponent],
  imports: [
    CommonModule,
    ApplicationRoutingModule,
    NgZorroAntdModule,
    ReactiveFormsModule,
    TranslateModule
  ]
})
export class ApplicationModule { }
