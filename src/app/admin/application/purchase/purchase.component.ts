import { Component, OnInit } from '@angular/core';
import { ApplicationService } from 'src/app/adminservices/application.service';
import { Application } from 'src/app/class/application';
import { User } from 'firebase';
import { NzModalService } from 'ng-zorro-antd';
import { ApplicationBySubscriptionService } from 'src/app/adminservices/applicationBySubscription.service';
import { ApplicationSubscription } from 'src/app/class/applicationSubscription';
import { SubscriptionService } from 'src/app/adminservices/subscription.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-purchase',
    templateUrl: './purchase.component.html',
    styleUrls: ['./purchase.component.scss']
})
export class PurchaseComponent implements OnInit {

    public applications = new Array<Application>();
    public user: User;
    public isVisible = false;
    public isOkLoading = false;
    public modalData = new Application();
    public applicationSubscription: ApplicationSubscription;

    constructor(
        private applicationService: ApplicationService,
        private subscriptionService: SubscriptionService,
        private applicationSubscriptionService: ApplicationBySubscriptionService,
        private modalService: NzModalService,
        private router: Router) { }

    ngOnInit() {
        this.user = JSON.parse(localStorage.getItem('firebase-user'));
        this.getApplications();
    }

    public getApplications() {
        this.applicationService.getApplications(this.user.uid).subscribe(
            (instance: any) => {
                this.applications = instance;
            }
        );
    }

    public createApplicationSuscription(applicationId) {
        this.subscriptionService.getSubscriptionByUserId(this.user.uid).subscribe(
            (subscription: any) => {
                console.log(subscription);
                this.applicationSubscription = new ApplicationSubscription();
                this.applicationSubscription.b4321 = subscription.b4321;
                this.applicationSubscription.k0871 = applicationId;
                console.log(this.applicationSubscription);
                this.applicationSubscriptionService.createApplicationSuscription(this.applicationSubscription).subscribe(
                    (instance: any) => {
                        this.closeModal();
                        if (instance.status) {
                            this.modalService.success({
                                nzTitle: 'Exito',
                                nzContent: instance.message
                                // nzContent: 'La operación se ha realizado exitosamente'
                            });
                            const appIndex = this.applications.findIndex(a => a.k0871 === applicationId);
                            this.applications.splice(appIndex, 1);
                        } else {
                            this.modalService.warning({
                                nzTitle: 'Warning',
                                nzContent: instance.message
                                // nzContent: 'La operación se ha realizado exitosamente'
                            });
                        }
                    }
                );
            }
        );
    }

    public openModalPurchase(application) {
        this.modalData = application;
        this.isVisible = true;
    }

    public closeModal(): void {
        this.isVisible = false;
        this.isOkLoading = false;
    }

    public handleOk(): void {
        this.isOkLoading = true;
        this.modalService.confirm(
            {
                nzTitle: '¿Esta Seguro?',
                nzOnOk: () => {
                    this.createApplicationSuscription(this.modalData.k0871);
                },
                nzOnCancel: () => {
                    this.closeModal();
                }
            });
    }

    public handleCancel(): void {
        this.isVisible = false;
    }
}
