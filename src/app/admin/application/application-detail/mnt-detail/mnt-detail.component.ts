import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from 'firebase';
import { SubscriptionService } from 'src/app/adminservices/subscription.service';
import { ApplicationBySubscriptionService } from 'src/app/adminservices/applicationBySubscription.service';
import { ApplicationService } from 'src/app/adminservices/application.service';
import { FormGroup, Validators, FormBuilder} from '@angular/forms';
import { AppComponentType, SubsComponent } from 'src/app/class/enums';
import { AppComponent } from 'src/app/class/appComponent';
import { AppSubsComponent } from 'src/app/class/appSubsComponent';
import { NzModalService, NzModalRef } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-mnt-detail',
    templateUrl: './mnt-detail.component.html',
    styleUrls: ['./mnt-detail.component.scss']
})
export class MntDetailComponent implements OnInit {

    public appSubscriptionId: string;
    public appComponents: Array<AppComponent>;
    public user: User;
    public k1: string;
    public k2: string;
    public k1Loading = false;
    public k2Loading = false;
    public saveLoading = false;
    public ivrForm: FormGroup;
    public selfServiceForm: FormGroup;
    public appMovilForm: FormGroup;
    public selfServiceFormDisable: boolean;
    public ivrFormDisable: boolean;
    public appMovilFormDisable: boolean;
    public componentType = AppComponentType;
    public confirmModal: NzModalRef;

    constructor(
        private activatedRoute: ActivatedRoute,
        private applicationService: ApplicationService,
        private suscriptionService: SubscriptionService,
        private applicationBySubscriptionService: ApplicationBySubscriptionService,
        private formBuilder: FormBuilder,
        private modalService: NzModalService,
        private translate: TranslateService) { }

    ngOnInit() {
        this.user = JSON.parse(localStorage.getItem('firebase-user'));
        this.activatedRoute.params.subscribe(parameters => {
            if (!parameters.uid) {
            } else {
                this.appSubscriptionId = parameters.uid;
                this.getMntComponents(this.appSubscriptionId);
                this.getK1();
                this.getK2();
                this.createIvrForm();
                this.createSelfServiceForm();
                this.createAppMovilForm();
            }
        });
    }

    public createIvrForm() {
        this.ivrForm = this.formBuilder.group({
            displayName: [null, [Validators.required]],
            userName: [null, [Validators.required]],
            password: [null, [Validators.required]],
            authorizationUserName: [null, [Validators.required]],
            domain: [null, [Validators.required]],
            port: [null, [Validators.required]],
            portType: [null, [Validators.required]]
        });
    }

    public createSelfServiceForm() {
        this.selfServiceForm = this.formBuilder.group({
            url: [null, [Validators.required]]
        });
    }

    public createAppMovilForm() {
        this.appMovilForm = this.formBuilder.group({
        });
    }

    public getMntComponents(appSubscriptionId: string) {
        this.applicationService.getAppComponents(appSubscriptionId).subscribe(
            (instance: any) => {
                this.appComponents = instance;
                this.appComponents.forEach(appComponent => {
                    if (!appComponent.subsComponents) {
                        switch (appComponent.c7054) {
                            case this.componentType.IVR:
                                this.ivrFormDisable = false;
                                this.ivrForm.enable();
                                this.ivrForm.updateValueAndValidity();
                                break;
                            case this.componentType.SELF_SERVICE:
                                this.selfServiceFormDisable = false;
                                this.selfServiceForm.enable();
                                this.selfServiceForm.updateValueAndValidity();
                                break;
                            case this.componentType.APP_MOVIL:
                                break;
                        }
                    } else {
                        switch (appComponent.c7054) {
                            case this.componentType.IVR:
                                this.ivrFormDisable = true;
                                this.ivrForm.disable();
                                this.ivrForm.updateValueAndValidity();
                                break;
                            case this.componentType.SELF_SERVICE:
                                this.selfServiceFormDisable = true;
                                this.selfServiceForm.disable();
                                this.selfServiceForm.updateValueAndValidity();
                                break;
                            case this.componentType.APP_MOVIL:
                                break;
                        }
                        this.processMntData(appComponent);
                    }
                });
                console.log(this.appComponents);
            }
        );
    }

    public processMntData(appComponent: AppComponent) {

        switch (appComponent.c7054) {
            case this.componentType.IVR:
                const displayName = appComponent.subsComponents.find(x => x.y9072 === SubsComponent.IVR_DISPLAY_NAME);
                const userName = appComponent.subsComponents.find(x => x.y9072 === SubsComponent.IVR_USERNAME);
                const authorizationUserName = appComponent.subsComponents.find(x => x.y9072 === SubsComponent.IVR_AUTHORIZATION_USERNAME);
                const password = appComponent.subsComponents.find(x => x.y9072 === SubsComponent.IVR_PASSWORD);
                const port = appComponent.subsComponents.find(x => x.y9072 === SubsComponent.IVR_PORT);
                const portType = appComponent.subsComponents.find(x => x.y9072 === SubsComponent.IVR_PORT_TYPE);
                const domain = appComponent.subsComponents.find(x => x.y9072 === SubsComponent.IVR_DOMAIN);
                this.ivrForm.setValue({
                    displayName: displayName === null ? null : displayName.y9075,
                    userName: userName === null ? null : userName.y9075,
                    password: password === null ? null : password.y9075,
                    authorizationUserName: authorizationUserName === null ? null : authorizationUserName.y9075,
                    domain: domain === null ? null : domain.y9075,
                    port: port === null ? null : port.y9075,
                    portType: portType === null ? null : portType.y9075
                });
                break;
            case this.componentType.SELF_SERVICE:
                const selfServiceUrl = appComponent.subsComponents.find(x => x.y9072 === SubsComponent.SELF_SERVICE_URL);
                this.selfServiceForm.setValue({
                    url: selfServiceUrl === null ? null : selfServiceUrl.y9075
                });
                break;
            case this.componentType.APP_MOVIL:
                break;
        }
    }

    public getK1() {
        this.k1Loading = true;
        this.suscriptionService.getK1(this.user.uid).subscribe(
            (instance: any) => {
                this.k1 = instance.encKCnfgData;
                this.k1Loading = false;
            },
            (error: any) => {
                this.k1Loading = false;
                console.log(error);
            }
        );
    }

    public getK2() {
        this.k2Loading = true;
        this.applicationBySubscriptionService.getK2(this.appSubscriptionId).subscribe(
            (instance: any) => {
                this.k2 = instance.encKCnfgData;
                this.k2Loading = false;
            },
            (error: any) => {
                this.k2Loading = false;
                console.log(error);
            }
        );
    }

    public regenerateK1() {
        this.k1Loading = true;
        this.suscriptionService.regenerateK1(this.user.uid).subscribe(
            (instance: any) => {
                this.k1 = instance.encKCnfgData;
                this.k1Loading = false;
            },
            (error: any) => {
                this.k1Loading = false;
                console.log(error);
            }
        );
    }

    public regenerateK2() {
        this.k2Loading = true;
        this.applicationBySubscriptionService.regenerateK2(this.appSubscriptionId).subscribe(
            (instance: any) => {
                this.k2 = instance.encKCnfgData;
                this.k2Loading = false;
            },
            (error: any) => {
                this.k2Loading = false;
                console.log(error);
            }
        );
    }

    public downloadAgent() {
        const link = document.createElement('a');
        link.href = 'data:application/octet-stream;base64,';
        link.download = 'Olympus_Universal_Moneta_Agent.exe';
        link.click();
    }

    public editConfiguration(appComponent: AppComponent) {

        switch (appComponent.c7054) {
            case this.componentType.IVR:
                if (this.ivrFormDisable) {
                    this.ivrForm.enable();
                    this.ivrFormDisable = false;
                } else {
                    this.ivrForm.disable();
                    this.ivrFormDisable = true;
                }
                this.ivrForm.updateValueAndValidity();
                break;
            case this.componentType.SELF_SERVICE:
                if (this.selfServiceFormDisable) {
                    this.selfServiceForm.enable();
                    this.selfServiceFormDisable = false;
                } else {
                    this.selfServiceForm.disable();
                    this.selfServiceFormDisable = true;
                }
                this.selfServiceForm.updateValueAndValidity();
                break;
            case this.componentType.APP_MOVIL:
                break;
        }
    }

    public saveConfiguration(appComponent: AppComponent) {
        switch (appComponent.c7054) {
            case this.componentType.IVR:
                this.createIvrComponents(appComponent);
                break;
            case this.componentType.SELF_SERVICE:
                this.createSelfServiceComponents(appComponent);
                break;
            case this.componentType.APP_MOVIL:
                break;
        }
    }

    public createIvrComponents(appComponent: AppComponent) {
        const listAppSubsComponent = new Array<AppSubsComponent>();

        listAppSubsComponent.push(
            this.createSubsComponent(
                appComponent.c7051,
                SubsComponent.IVR_DISPLAY_NAME,
                'Display Name', 'Display Name', this.displayName.value));
        listAppSubsComponent.push(
            this.createSubsComponent(
                appComponent.c7051,
                SubsComponent.IVR_USERNAME,
                'UserName', 'UserName', this.userName.value));
        listAppSubsComponent.push(
            this.createSubsComponent(
                appComponent.c7051,
                SubsComponent.IVR_AUTHORIZATION_USERNAME,
                'Authorization UserName', 'Authorization UserName', this.authorizationUserName.value));
        listAppSubsComponent.push(
            this.createSubsComponent(
                appComponent.c7051,
                SubsComponent.IVR_PASSWORD,
                'Password', 'Password', this.password.value));
        listAppSubsComponent.push(
            this.createSubsComponent(
                appComponent.c7051,
                SubsComponent.IVR_DOMAIN,
                'Domain', 'Domain', this.domain.value));
        listAppSubsComponent.push(
            this.createSubsComponent(
                appComponent.c7051,
                SubsComponent.IVR_PORT,
                'Port', 'Port', this.port.value));
        listAppSubsComponent.push(
            this.createSubsComponent(
                appComponent.c7051,
                SubsComponent.IVR_PORT_TYPE,
                'Port Type', 'Port Type', this.portType.value));
        this.createAppSubsComponent(listAppSubsComponent);
    }

    public createSelfServiceComponents(appComponent: AppComponent) {
        const listAppSubsComponent = new Array<AppSubsComponent>();
        listAppSubsComponent.push(
            this.createSubsComponent(
                appComponent.c7051,
                SubsComponent.SELF_SERVICE_URL,
                'Url', 'Url', this.url.value));
        this.createAppSubsComponent(listAppSubsComponent);
    }

    public createAppSubsComponent(listAppSubsComponent: Array<AppSubsComponent>) {
        this.applicationBySubscriptionService.createAppSubsComponent(listAppSubsComponent).subscribe(
            (instance: any) => {
                if (instance.status) {
                    this.modalService.success({
                        nzTitle: 'Exito',
                        nzContent: instance.message
                        // nzContent: 'La operación se ha realizado exitosamente'
                    });
                } else {
                    this.modalService.warning({
                        nzTitle: 'Warning',
                        nzContent: instance.message
                        // nzContent: 'La operación se ha realizado exitosamente'
                    });
                }
                this.saveLoading = false;
                this.disableForms();
            }
        );
    }

    public disableForms() {
        this.ivrForm.disable();
        this.ivrFormDisable = true;
        this.selfServiceForm.disable();
        this.selfServiceFormDisable = true;
        this.appMovilForm.disable();
        this.appMovilFormDisable = true;
    }

    public createSubsComponent(
        componentId: string,
        componentType: number,
        name: string,
        shortName: string,
        value: string): AppSubsComponent {

        const appSubsComponent = new AppSubsComponent();
        appSubsComponent.w0791 = this.appSubscriptionId;
        appSubsComponent.c7051 = componentId;
        appSubsComponent.y9072 = componentType;
        appSubsComponent.y9073 = name;
        appSubsComponent.y9074 = shortName;
        appSubsComponent.y9075 = value;
        appSubsComponent.y9076 = true;

        return appSubsComponent;
    }

    public showConfirm(appComponent: AppComponent): void {
        this.saveLoading = true;
        this.confirmModal = this.modalService.confirm({
            nzTitle: this.translate.instant('ADMIN.MODAL_CONFIRM_TITTLE'),
            nzOkText: this.translate.instant('ADMIN.MODAL_SAVE_TEXT'),
            nzCancelText: this.translate.instant('ADMIN.MODAL_CANCEL_TEXT'),
            nzContent: '',
            nzOnOk: () => {
                this.saveConfiguration(appComponent);
            },
            nzOnCancel: () => {
                this.saveLoading = false;
            }
        });
    }

    get displayName() {
        return this.ivrForm.get('displayName');
    }
    get userName() {
        return this.ivrForm.get('userName');
    }
    get password() {
        return this.ivrForm.get('password');
    }
    get authorizationUserName() {
        return this.ivrForm.get('authorizationUserName');
    }
    get domain() {
        return this.ivrForm.get('domain');
    }
    get port() {
        return this.ivrForm.get('port');
    }
    get portType() {
        return this.ivrForm.get('portType');
    }

    get url() {
        return this.selfServiceForm.get('url');
    }

    // descargarPlantilla() {
    //     this.archivoService.bajarArchivo({ Nombre: this.entidadParametro.Valor, Base64: '' }).subscribe(
    //         (instance: any) => {
    //             const link = document.createElement('a');
    //             link.href = 'data:application/octet-stream;base64,' + instance.Base64;
    //             switch (this.entidadParametro.IdParametro) {
    //                 case Parametro.PLANTILLA_MASIVO:
    //                     link.download = instance.Nombre + '.xlsx';
    //                     break;
    //                 case Parametro.LOGO:
    //                     link.download = instance.Nombre + '.png';
    //                     break;
    //                 default:
    //                     link.download = instance.Nombre + '.xml';
    //                     break;
    //             }
    //             link.click();
    //         }
    //     );
    // }

}
