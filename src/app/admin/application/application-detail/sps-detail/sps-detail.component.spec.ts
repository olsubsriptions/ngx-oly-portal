import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpsDetailComponent } from './sps-detail.component';

describe('SpsDetailComponent', () => {
  let component: SpsDetailComponent;
  let fixture: ComponentFixture<SpsDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpsDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
