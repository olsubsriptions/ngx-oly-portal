import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TtsDetailComponent } from './tts-detail.component';

describe('TtsDetailComponent', () => {
  let component: TtsDetailComponent;
  let fixture: ComponentFixture<TtsDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TtsDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TtsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
