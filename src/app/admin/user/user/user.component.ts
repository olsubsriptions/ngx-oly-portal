import { Component, OnInit } from '@angular/core';
import { User } from 'firebase';
import { UserService } from 'src/app/adminservices/user.service';
import { NzModalService } from 'ng-zorro-antd';
import { UserS } from 'src/app/class/userS';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

    public users = new Array<UserS>();
    public user: User;

    constructor(
        private userService: UserService,
        private modalService: NzModalService) { }

    ngOnInit() {
        this.user = JSON.parse(localStorage.getItem('firebase-user'));
        this.getUsers();
    }

    public getUsers() {
        this.userService.getUsers(this.user.uid).subscribe(
            (instance: any) => {
                this.users = instance;
                console.log(this.users);
            }
        );
    }

}
