import { Component, OnInit } from '@angular/core';
import { ApplicationService } from 'src/app/adminservices/application.service';
import { Application } from 'src/app/class/application';
import { AppComponent } from 'src/app/class/appComponent';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppInstance } from 'src/app/class/appInstance';
import { NzModalService } from 'ng-zorro-antd';

@Component({
    selector: 'app-setup',
    templateUrl: './setup.component.html',
    styleUrls: ['./setup.component.scss']
})
export class SetupComponent implements OnInit {

    public applications: Array<Application>;
    public instanceForm: FormGroup;
    public isOkLoading = false;
    public isVisible = false;
    public modalData = new AppInstance();
    public modalTitle: string;

    constructor(
        private applicationService: ApplicationService,
        private modalService: NzModalService,
        private formBuilder: FormBuilder) { }

    ngOnInit() {
        this.getAllApplications();
        this.createInstanceForm();
    }

    public getAllApplications() {
        this.applicationService.getAllApplications().subscribe(
            (instance: any) => {
                this.applications = instance;
                console.log(instance);
            }
        );
    }

    public createApplicationInstance(appInstance: AppInstance) {

        appInstance.i7072 = this.url.value;
        appInstance.i7073 = this.port.value;
        appInstance.i7074 = JSON.parse(this.state.value);
        appInstance.i7076 = this.dbuser.value;
        appInstance.i7077 = this.dbpass.value;
        this.resetInstanceForm();

        if (appInstance.i7071) {

            this.applicationService.updateInstance(appInstance).subscribe(
                (instance: any) => {
                    if (instance.status) {
                        this.modalService.success({
                            nzTitle: 'Exito',
                            nzContent: instance.message
                            // nzContent: 'La operación se ha realizado exitosamente'
                        });

                        const application = this.applications.find(x => x.k0871 === appInstance.k0871);
                        const applicationInstance = application.instances.find(x => x.i7071 === appInstance.i7071);

                        const index = application.instances.indexOf(applicationInstance);
                        application.instances.splice(index, 1);
                        application.instances.push(appInstance);

                    } else {
                        this.modalService.warning({
                            nzTitle: 'Warning',
                            nzContent: instance.message
                            // nzContent: 'La operación se ha realizado exitosamente'
                        });
                    }
                }
            );

        } else {
            appInstance.i7075 = 0;

            this.applicationService.createInstance(appInstance).subscribe(
                (instance: any) => {
                    if (instance.status) {
                        this.modalService.success({
                            nzTitle: 'Exito',
                            nzContent: instance.message
                            // nzContent: 'La operación se ha realizado exitosamente'
                        });
                        this.applications.find(x => x.k0871 === appInstance.k0871).instances.push(appInstance);
                    } else {
                        this.modalService.warning({
                            nzTitle: 'Warning',
                            nzContent: instance.message
                            // nzContent: 'La operación se ha realizado exitosamente'
                        });
                    }
                }
            );
        }
    }

    public createInstanceForm() {
        this.instanceForm = this.formBuilder.group({
            url: [null, [Validators.required]],
            port: [null, [Validators.required]],
            state: [null, [Validators.required]],
            dbuser: [null, [Validators.required]],
            dbpass: [null, [Validators.required]]
        });
    }

    public resetInstanceForm() {
        this.instanceForm.setValue({
            url: null,
            port: null,
            state: null,
            dbuser: null,
            dbpass: null
        });
    }

    public changeComponentStatus(component: AppComponent) {
        if (component.c7053) {
            component.c7053 = false;
        } else {
            component.c7053 = true;
        }
    }

    public editInstance(appInstance: AppInstance) {
        this.modalTitle = 'ADMIN.CONFIGURATION.EDIT_INSTANCE';
        this.modalData = appInstance;
        this.instanceForm.setValue({
            url: this.modalData.i7072,
            port: this.modalData.i7073,
            state: this.modalData.i7074,
            dbuser: this.modalData.i7076,
            dbpass: this.modalData.i7077
        });
        this.isVisible = true;
    }

    public createInstance(application: Application) {
        this.modalTitle = 'ADMIN.CONFIGURATION.NEW_INSTANCE';
        this.modalData = new AppInstance();
        this.modalData.k0871 = application.k0871;
        this.isVisible = true;
    }

    public closeModal(): void {
        this.isVisible = false;
        this.isOkLoading = false;
    }

    public handleOk(): void {
        this.isOkLoading = true;
        this.modalService.confirm(
            {
                nzTitle: '¿Esta Seguro?',
                nzOnOk: () => {
                    this.createApplicationInstance(this.modalData);
                    this.closeModal();
                },
                nzOnCancel: () => {
                    this.closeModal();
                }
            });
    }

    public handleCancel(): void {
        this.isVisible = false;
    }

    // url: [null, [Validators.required]],
    // port: [null, [Validators.required]],
    // state: [null, [Validators.required]],
    // dbuser: [null, [Validators.required]],
    // dbpass

    get url() {
        return this.instanceForm.get('url');
    }
    get port() {
        return this.instanceForm.get('port');
    }
    get state() {
        return this.instanceForm.get('state');
    }
    get dbuser() {
        return this.instanceForm.get('dbuser');
    }
    get dbpass() {
        return this.instanceForm.get('dbpass');
    }
}
