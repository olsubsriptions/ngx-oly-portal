import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BillingRoutingModule } from './billing-routing.module';
import { PaymentComponent } from './payment/payment.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';


@NgModule({
  declarations: [PaymentComponent],
  imports: [
    CommonModule,
    BillingRoutingModule,
    NgZorroAntdModule
  ]
})
export class BillingModule { }
