import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PortalComponent} from './portal.component';
import {NavigationComponent} from './navigation/navigation.component';
import {WelcomeComponent} from './zones/welcome/welcome.component';
import {SharedModule} from '../core/shared.module';
import {CustomersComponent} from './zones/customers/customers.component';
import {ProductsComponent} from './zones/products/products.component';
import {PricesComponent} from './zones/prices/prices.component';
import {CommunityComponent} from './zones/community/community.component';
import {FooterComponent} from './zones/footer/footer.component';
import {LinkItemComponent} from './zones/footer/link-item/link-item.component';
import {PortalRoutingModule} from './portal-routing.module';
import { LoginComponent } from './zones/login/login.component';

@NgModule({
  declarations: [
    PortalComponent,
    NavigationComponent,
    WelcomeComponent,
    CustomersComponent,
    ProductsComponent,
    PricesComponent,
    CommunityComponent,
    FooterComponent,
    LinkItemComponent,
    LoginComponent,
  ],
  imports: [
    CommonModule, SharedModule, PortalRoutingModule
  ]
})
export class PortalModule {
}
