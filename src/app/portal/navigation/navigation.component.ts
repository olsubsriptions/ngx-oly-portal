import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NzDirectionVHIType} from 'ng-zorro-antd';
import {MenuItem} from './navigation.model';


@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  @Input()
  direction: NzDirectionVHIType;

  @Input()
  theme: 'light' | 'dark';

  @Input()
  isCollapsed: boolean;

  @Input()
  menuData: MenuItem[];

  @Output() clickOnItem: EventEmitter<string> = new EventEmitter<string>();

  constructor() {
    this.direction = 'inline';
    this.theme = 'dark';
    this.isCollapsed = true;
  }

  ngOnInit() {
  }
}
