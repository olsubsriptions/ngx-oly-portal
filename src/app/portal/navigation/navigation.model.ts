export class MenuItem {
  title: string;
  elementId?: string;
  child?: MenuItem[];
}
