export class LinkItem {
  href: string;
  label: string;
}
