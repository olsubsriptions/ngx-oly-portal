import {Component, OnInit} from '@angular/core';
import {LinkItem} from './link-item/link-item';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  dataA: LinkItem[] = [
    {
      label: 'PRINCIPAL.FOOTER.A.S1',
      href: ''
    },
    {
      label: 'PRINCIPAL.FOOTER.A.S2',
      href: ''
    },
    {
      label: 'PRINCIPAL.FOOTER.A.S3',
      href: ''
    }
  ];

  dataB: LinkItem[] = [
    {
      label: 'PRINCIPAL.FOOTER.B.S1',
      href: ''
    },
    {
      label: 'PRINCIPAL.FOOTER.B.S2',
      href: ''
    },
    {
      label: 'PRINCIPAL.FOOTER.B.S3',
      href: ''
    },
    {
      label: 'PRINCIPAL.FOOTER.B.S4',
      href: ''
    }
  ];

  dataC: LinkItem[] = [
    {
      label: 'PRINCIPAL.FOOTER.C.S1',
      href: ''
    },
    {
      label: 'PRINCIPAL.FOOTER.C.S2',
      href: ''
    }
  ];

  dataD: LinkItem[] = [
    {
      label: 'PRINCIPAL.FOOTER.D.S1',
      href: ''
    },
    {
      label: 'PRINCIPAL.FOOTER.D.S2',
      href: ''
    }
  ];

  constructor() {
  }

  ngOnInit() {
  }

}
