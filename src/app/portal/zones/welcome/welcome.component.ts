import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {
  bannerText: string[];
  validateForm: FormGroup;
  submitted = false;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.bannerText = [
      'PRINCIPAL.BANNER.A',
      'PRINCIPAL.BANNER.B',
      'PRINCIPAL.BANNER.C',
      'PRINCIPAL.BANNER.D',
      'PRINCIPAL.BANNER.E',
    ];

    this.validateForm = this.fb.group({
      username: [null, [Validators.required]],
      domain: [null, [Validators.required]]
    });
  }

  submitForm(): void {
    for (const field in this.validateForm.controls) {
      if (this.validateForm.controls[field]) {
        this.validateForm.controls[field].markAsDirty();
        this.validateForm.controls[field].updateValueAndValidity();
      }
    }
    this.submitted = true;
  }

}
