import {Component, OnInit} from '@angular/core';

interface ImageItem {
  source: string;
}

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  data: ImageItem[] = [
    {
      source: 'assets/img/products/moneta.png'
    },
    {
      source: 'assets/img/products/sps.png'
    },
    {
      source: 'assets/img/products/tts.png'
    }
  ];

  constructor() {
  }

  ngOnInit() {
  }

}
