import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzSizeLDSType } from 'ng-zorro-antd';
import { Router } from '@angular/router';
import { RegisterService } from '../register.service';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit {
  errorMessage: string;
  companyDataForm: FormGroup;
  disabled = false;
  inputSize: NzSizeLDSType = 'large';
  firstState: any = {};
  nextPage: string[] = ['/commons/billing'];
  lastPage: string[] = ['/commons/register'];

  constructor(private fb: FormBuilder, private router: Router, private registerService: RegisterService) {
    const companyData = this.registerService.state3 || {};
    this.companyDataForm = this.fb.group({
      company: [companyData.company, [Validators.required]],
      document: [companyData.document, [Validators.required]],
      state: [companyData.state, [Validators.required]],
      province: [companyData.province, [Validators.required]],
      city: [companyData.city, [Validators.required]],
      address1: [companyData.address1, [Validators.required]],
      address2: [companyData.address2, [Validators.required]],
    });
  }

  ngOnInit() {
    this.firstState = this.registerService.state1;
    if (!this.firstState) {
      this.back();
      return;
    }
  }

  submitForm(): void {
    this.disabled = true;
    if (this.companyDataForm.valid) {
      this.registerService.state3 = this.companyDataForm.getRawValue();
      this.router.navigate(this.nextPage).then();
    } else {
      for (const field in this.companyDataForm.controls) {
        if (this.companyDataForm.controls[field]) {
          this.companyDataForm.controls[field].markAsDirty();
          this.companyDataForm.controls[field].updateValueAndValidity();
        }
      }
    }
    this.enableButton();
  }

  enableButton(milliseconds: number = 2000): void {
    setTimeout(() => {
      this.disabled = false;
    }, milliseconds);
  }

  skip() {
    this.registerService.state3 = null;
    this.router.navigate(this.nextPage).then();
  }

  back() {
    this.router.navigate(this.lastPage).then();
  }
}
