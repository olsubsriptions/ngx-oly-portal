import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  public state1: any;
  // public state2: any;
  public state3: any;
  public state4: any;
  apiUrl = environment.api.url;

  constructor(private http: HttpClient) {
  }

  clearStates() {
    this.state1 = null;
    // this.state2 = null;
    this.state3 = null;
    this.state4 = null;
  }

  preValidateLoginData(loginData: any) {
    // return this.http.post<any>(`${this.apiUrl}/common/validate/register`, {login: loginData});
    return this.http.post<any>(`${this.apiUrl}/users/validate/register`, loginData);
  }

  validateStates() {
    return (this.state1);
  }

  createUser() {
    return this.http.post<any>(`${this.apiUrl}/users`, {
      login: this.state1,
      // user: this.state2,
      company: this.state3,
      billing: this.state4
    });
  }
}
