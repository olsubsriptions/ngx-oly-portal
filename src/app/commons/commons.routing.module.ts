import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CommonsComponent} from './commons.component';
import {ValidateComponent} from './validate/validate.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register/register.component';
import {CompanyComponent} from './register/company/company.component';
import {BillingComponent} from './register/billing/billing.component';
import {ConfirmComponent} from './register/confirm/confirm.component';

const routes: Routes = [
  {
    path: 'commons',
    component: CommonsComponent,
    children: [
      {
        path: '',
        component: ValidateComponent
      },
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'register',
        component: RegisterComponent
      },
      {
        path: 'organization',
        component: CompanyComponent
      },
      {
        path: 'billing',
        component: BillingComponent
      },
      {
        path: 'confirm',
        component: ConfirmComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class CommonsRoutingModule {
}
