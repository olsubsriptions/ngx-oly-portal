import {NgModule} from '@angular/core';

import {SharedModule} from '../core/shared.module';

import {CommonsRoutingModule} from './commons.routing.module';

import {ValidateComponent} from './validate/validate.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register/register.component';
import {CompanyComponent} from './register/company/company.component';
import {BillingComponent} from './register/billing/billing.component';
import {ConfirmComponent} from './register/confirm/confirm.component';

import {AutofocusDirective} from '../core/autofocus.directive';
import {CommonsComponent} from './commons.component';

@NgModule({
  declarations: [
    AutofocusDirective,
    ValidateComponent,
    LoginComponent,
    RegisterComponent,
    CommonsComponent,
    CompanyComponent,
    BillingComponent,
    ConfirmComponent],
  exports: [],
  imports: [
    SharedModule,
    CommonsRoutingModule
  ]
})
export class CommonsModule {
}
