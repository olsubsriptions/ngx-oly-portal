import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-commons',
  animations: [],
  templateUrl: './commons.component.html',
  styleUrls: ['./commons.component.scss']
})
export class CommonsComponent implements OnInit {

  constructor(private translate: TranslateService) {
    translate.setDefaultLang('es');
    if (localStorage.getItem('portal-curr-lang')) {
      translate.use(localStorage.getItem('portal-curr-lang'));
    } else {
      translate.use('es');
    }
  }

  changeLanguage() {
    const currentLang = this.translate.currentLang;
    if (currentLang === 'es') {
      this.translate.use('de');
      localStorage.setItem('portal-curr-lang', 'de');
    } else {
      this.translate.use('es');
      localStorage.setItem('portal-curr-lang', 'es');
    }
    window.location.reload();
  }

  ngOnInit() {
  }
}
