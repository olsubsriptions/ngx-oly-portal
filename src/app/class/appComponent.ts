import { AppSubsComponent } from './appSubsComponent';

export class AppComponent {
    c7051: string; // ouid
    k0871: string; // applicationId
    c7052: string; // componentName
    c7053: boolean; // state
    c7054: number; // componentType
    subsComponents: Array<AppSubsComponent>; // listAppSubsComponent
}
