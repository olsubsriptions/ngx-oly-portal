import { AppComponent } from './appComponent';
import { AppInstance } from './appInstance';

export class Application {
    k0871: string; // ouid
    u4321: string; // profileID
    k0872: string; // applicationName
    k0873: string; // applicationShortName
    k0874: string; // version
    k0875: string; // versionMajor
    k0876: string; // versionMinor
    k0877: string; // applicationEndpoint
    k0878: string; // port
    k0879: boolean; // state
    k0880: string; // image
    components: Array<AppComponent>; // components
    instances: Array<AppInstance>; // instances
}
