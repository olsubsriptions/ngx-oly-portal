import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpRequest } from '@angular/common/http';
import { HttpInterceptor } from '@angular/common/http';
import { Observable, EMPTY, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { NzModalService } from 'ng-zorro-antd';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    public user: any;
    public angularFireAuth: AngularFireAuth;
    public token: string;
    constructor(
        private router: Router,
        private modalService: NzModalService) {
    }


    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const user = localStorage.getItem('firebase-user');
        let newRequest: any;
        if (user) {
            this.user = JSON.parse(user);
            this.token = this.user.stsTokenManager.accessToken;
            newRequest = request.clone({
                setHeaders: {
                    Authorization: ('Bearer ' + this.token)
                }
            });
        } else {
            newRequest = request;
        }
        console.log(newRequest);
        return next.handle(newRequest).pipe(
            catchError(error => {
                if (error.status === 401) {
                    this.modalService.warning({
                        nzTitle: 'Atención',
                        nzContent: error.error.message
                    });
                    localStorage.removeItem('firebase-user');
                    this.router.navigateByUrl('/commons');
                    return EMPTY;
                } else {
                    this.modalService.error({
                        nzTitle: 'Ha ocurrido un error',
                        nzContent: error.error.message
                    });
                    return throwError(error);
                }
            })
        );
    }
}
