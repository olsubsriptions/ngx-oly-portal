export const environment = {
  production: true,
  api: {
    url: 'https://olsportalapi-dot-subs-001.appspot.com/'
  },
  console: {
    url: 'https://olsconsoleapi-dot-subs-001.appspot.com'
  },
  firebase: {
    apiKey: 'AIzaSyC_99yvJeqd94fxXwnZrmjeTQAv9JQTG5k',
    authDomain: 'subs-001.firebaseapp.com',
    databaseURL: 'https://subs-001.firebaseio.com',
    projectId: 'subs-001',
    storageBucket: 'subs-001.appspot.com',
    messagingSenderId: '25713393184',
    appId: '1:25713393184:web:56c32fdfe06fd8a761c645',
    measurementId: 'G-CSS3KCLSRH'
  }
};
