// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    api: {
        // url: 'http://localhost:8100'
        url: 'https://olsportalapi-dot-subs-001.appspot.com/'
    },
    console: {
        url: 'http://localhost:8080'
    },
    firebase: {
        apiKey: 'AIzaSyC_99yvJeqd94fxXwnZrmjeTQAv9JQTG5k',
        authDomain: 'subs-001.firebaseapp.com',
        databaseURL: 'https://subs-001.firebaseio.com',
        projectId: 'subs-001',
        storageBucket: 'subs-001.appspot.com',
        messagingSenderId: '25713393184',
        appId: '1:25713393184:web:56c32fdfe06fd8a761c645',
        measurementId: 'G-CSS3KCLSRH'
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
